FROM registry.alpinelinux.org/alpine/infra/docker/golang:latest AS pipeline

COPY --chown=build:build pipeline /home/build/pipeline

WORKDIR /home/build/pipeline

RUN go build -ldflags='-s -w' .

FROM registry.alpinelinux.org/alpine/infra/docker/build-base:edge

COPY overlay/ /
COPY --from=pipeline /home/build/pipeline/pipeline /usr/local/bin

RUN git config --global init.defaultBranch master
RUN doas mkdir /apkcache && doas ln -sv /apkcache /etc/apk/cache

ENV SUDO=doas
