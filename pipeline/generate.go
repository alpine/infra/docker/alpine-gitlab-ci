package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"slices"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

var generateCommand = &cobra.Command{
	Use:           "generate <pipeline-template> <config>",
	Short:         "Generate a pipeline definition",
	SilenceErrors: true,
	SilenceUsage:  true,
	RunE:          runGenerate,
	Args:          cobra.ExactArgs(2),
}

func runGenerate(cmd *cobra.Command, args []string) error {
	pipelineTemplatePath := args[0]
	configPath := args[1]

	repoPath := LookupEnvWithDefault("CI_PROJECT_DIR", ".")
	targetBranch := LookupEnvWithDefault("CI_MERGE_REQUEST_TARGET_BRANCH_NAME", "master")
	upstreamUrl := LookupEnvWithDefault("CI_MERGE_REQUEST_PROJECT_URL", "https://gitlab.alpinelinux.org/alpine/aports.git")

	configFile, err := os.Open(configPath)
	if err != nil {
		return fmt.Errorf("could not open file '%s': %w", configPath, err)
	}
	defer configFile.Close()

	jobConfig, err := ParseConfig(configFile)
	if err != nil {
		return fmt.Errorf("failed to parse config: %w", err)
	}

	pipelineTemplateFile, err := os.Open(pipelineTemplatePath)
	if err != nil {
		return fmt.Errorf("could not open '%s': %w", pipelineTemplatePath, err)
	}

	pipelineTemplateContents, err := io.ReadAll(pipelineTemplateFile)
	if err != nil {
		return fmt.Errorf("could not read '%s': %w", pipelineTemplatePath, err)
	}

	pipelineDefinition := map[string]any{}
	err = yaml.Unmarshal(pipelineTemplateContents, &pipelineDefinition)
	if err != nil {
		return fmt.Errorf("could not parse '%s' as yaml: %w", pipelineTemplatePath, err)
	}

	repo, err := prepareRepo(repoPath, targetBranch, upstreamUrl)
	if err != nil {
		return fmt.Errorf("failed to prepare repo: %w", err)
	}

	changedAports, err := changedAports(repo, targetBranch)
	if err != nil {
		return err
	}

	applicableRules := ruleMatches(changedAports, jobConfig)

	for _, rule := range applicableRules {
		for _, tag := range rule.Actions.AddTags {
			pipelineDefinition = addTagToJobs(pipelineDefinition, tag)
		}
	}

	out, err := yaml.Marshal(pipelineDefinition)
	if err != nil {
		return fmt.Errorf("could not marshal data: %w", err)
	}
	fmt.Println(string(out))
	return nil
}

func ruleMatches(changedAports []string, packageRules Config) []PackageRule {
	matchingRules := []PackageRule{}
	for _, rule := range packageRules {
		if len(rule.Conditions.ChangedAports) > 0 {
			changedAportsMatches := false
			for _, aport := range changedAports {
				if slices.Contains(rule.Conditions.ChangedAports, aport) {
					changedAportsMatches = true
					break
				}
			}
			if !changedAportsMatches {
				continue
			}
		}

		if rule.Conditions.NrChanges > 0 {
			if len(changedAports) < rule.Conditions.NrChanges {
				continue
			}
		}

		matchingRules = append(matchingRules, rule)
	}

	return matchingRules
}

func addTagToJobs(jobs map[string]any, tag string) map[string]any {
	adjustedDefinition := map[string]any{}
	for key, job := range jobs {
		adjustedDefinition[key] = job

		var jobMap map[string]any
		var tags any
		var tagList []any
		var ok, hasTags bool

		if jobMap, ok = job.(map[string]any); !ok {
			continue
		}
		if tags, hasTags = jobMap["tags"]; !hasTags {
			continue
		}
		if tagList, ok = tags.([]any); !ok {
			continue
		}
		tagList = append(tagList, tag)
		jobMap["tags"] = tagList
	}
	return jobs
}

// prepareRepo opens the repository and does some preparation. Specifically it
// makes sure the target branch is available from the provided remote.
// This is because gitlab clones only a single branch, so we cannot compare
// the current branch with the target branch.
func prepareRepo(path, targetBranch, remoteUrl string) (*git.Repository, error) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return nil, fmt.Errorf("could not open repo: %w", err)
	}

	remote, err := repo.CreateRemoteAnonymous(&config.RemoteConfig{
		Name: "anonymous",
		URLs: []string{remoteUrl},
		Fetch: []config.RefSpec{
			config.RefSpec(fmt.Sprintf("+refs/heads/%[1]s:refs/heads/%[1]s", targetBranch)),
		},
	})
	if err != nil {
		return nil, fmt.Errorf("could not create anonymous remote: %w", err)
	}

	err = remote.Fetch(&git.FetchOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch '%s' from '%s': %w", targetBranch, remoteUrl, err)
	}

	return repo, nil
}

func changedAports(repo *git.Repository, targetBranch string) ([]string, error) {
	headRef, err := repo.Head()
	if err != nil {
		return nil, fmt.Errorf("could not obtain HEAD: %w", err)
	}

	headCommit, err := repo.CommitObject(headRef.Hash())
	if err != nil {
		return nil, fmt.Errorf("cannot obtain commit object for HEAD: %w", err)
	}

	targetRefHash, err := repo.ResolveRevision(plumbing.Revision(targetBranch))
	if err != nil {
		return nil, fmt.Errorf("could not resolve '%s': %w", targetBranch, err)
	}

	targetBranchCommit, err := repo.CommitObject(*targetRefHash)
	if err != nil {
		return nil, fmt.Errorf("cannot obtain commit object for HEAD: %w", err)
	}

	mergeBase, err := targetBranchCommit.MergeBase(headCommit)
	if err != nil {
		return nil, fmt.Errorf("could not determine mergebase between HEAD and %s: %w", targetBranch, err)
	}

	mergeBaseCommit, err := repo.CommitObject(mergeBase[0].Hash)
	if err != nil {
		return nil, fmt.Errorf("could not obtain commit object for merge base %s: %w", mergeBase[0].Hash, err)
	}

	mergeBaseTree, err := repo.TreeObject(mergeBaseCommit.TreeHash)
	if err != nil {
		return nil, fmt.Errorf("could not obtain tree object for merge base commit %s: %w", mergeBaseCommit.Hash, err)
	}

	headTree, err := repo.TreeObject(headCommit.TreeHash)
	if err != nil {
		return nil, fmt.Errorf("could not obtain tree object for head commit: %w", err)
	}

	changes, err := object.DiffTree(mergeBaseTree, headTree)
	if err != nil {
		return nil, fmt.Errorf("could not calculate differences between merge base and target branch: %w", err)
	}

	changedAports := []string{}
	for _, change := range changes {
		filename := filepath.Base(change.To.Name)
		pkgname := filepath.Base(filepath.Dir(change.To.Name))
		if filename == "APKBUILD" {
			changedAports = append(changedAports, pkgname)
		}
	}

	return changedAports, nil
}

type Condition struct {
	ChangedAports []string `yaml:"changed_aports"`
	NrChanges     int      `yaml:"nr_changes"`
}

type Action struct {
	AddTags []string `yaml:"add_tags"`
}

type PackageRule struct {
	Conditions Condition `yaml:"conditions"`
	Actions    Action    `yaml:"actions"`
}

type Config []PackageRule

func ParseConfig(r io.Reader) (Config, error) {
	contents, err := io.ReadAll(r)
	if err != nil {
		return Config{}, fmt.Errorf("could not read job config: %w", err)
	}

	var config Config
	err = yaml.Unmarshal(contents, &config)
	if err != nil {
		return Config{}, fmt.Errorf("could not parse job config: %w", err)
	}
	return config, nil
}
