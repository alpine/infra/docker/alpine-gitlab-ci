package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use: "pipeline",
}

func main() {
	rootCmd.AddCommand(
		generateCommand,
	)
	err := rootCmd.Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "err: %s\n", err)
		os.Exit(1)
	}
}

func LookupEnvWithDefault(name, defaultValue string) string {
	if value, ok := os.LookupEnv(name); ok {
		return value
	} else {
		return defaultValue
	}
}
